//
//  APIModule.swift
//  CricketLive
//
//  Created by Ios_Team on 02/04/21.
//  Copyright © 2021 Techathalon. All rights reserved.
//

import Foundation


class APIModule: NSObject {
    
    static func emailLogin(by urlString : String , para : Data, httpMeethodType : String){
        
    }
    
    
    static func getDictionaryFromModel(model:MatchDetailModel) -> [String :Any]? {
        if let JsonObject = try?  JSONEncoder().encode(model){
            if let dictionary = try? JSONSerialization.jsonObject(with: JsonObject, options: .allowFragments) as? [String:Any]{
                return dictionary
            }
        }
        return nil
    }
    
    
    func getDataFromAPI(apiString:String , completion: @escaping (_ responseDataString:String , _ status:Bool) -> Void)  {
        
        
        let urlString = String(format: apiString)
        
        let serviceUrl = URL(string: urlString)
        
        
        var request = URLRequest(url: serviceUrl!)
        
        request.httpMethod = MethodType.GET.rawValue
        
        request.setValue(HeaderValue.Json.rawValue, forHTTPHeaderField: HeaderKey.ContentType.rawValue)
        
        printLogger("send otp request-->\(request)")
        
        let session = URLSession.shared
        session.dataTask(with: request) { (data, response, error) in
            
            if let error = error{
                printLogger(error)
                completion(error.localizedDescription,false)
                
            }else{
                if let response = response {
                    printLogger(response)
                }
                if let data1 = data {
                    printLogger(data1)
                    let responseString:String = String(data: data1, encoding: String.Encoding.utf8)!
                    completion(responseString,true)
                }
            }
            
        }.resume()
        
    }
    
    
    class func showFailureAlert() {
        AppUtility.displayAlert(title: "Failure", message: "Some error is occurred!")
    }
    
    class func getApiDataInDict<T:Decodable>(methodName :String?,headerFieldVal :String?,pathParam :String?,jsonParam:Dictionary<String,Any>?, headers:Dictionary<String, Any>?,requestURL:String,returnInCaseOffailure:Bool,decodingType: T.Type,completion : @escaping(_ status:Bool,_ model:T?) -> Void)
    {
        LoadingIndicatorView.show()
    var request = URLRequest(url: URL(string: requestURL)!)
    request.httpMethod = methodName!
        request.addValue(headerFieldVal!, forHTTPHeaderField: "Content-Type")

    let session = URLSession.shared
    let task = session.dataTask(with: request, completionHandler: { data, response, error -> Void in
        guard let data = data else { return }
        do{
            let model = try JSONDecoder().decode(T.self, from: data)
            completion(true, model)
        }catch{
         printLogger("CATCH")
        }
        DispatchQueue.main.async {
            LoadingIndicatorView.hide()
        }
    })

    task.resume()
    }
    
    /*
    class func getApiDataInDict(requestType :String, queryParam:String?,pathParam :String?,jsonParam:Dictionary<String,Any>?, headers:Dictionary<String, Any>?,requestURL:String,returnInCaseOffailure:Bool,completion : @escaping(_ status:Bool,_ response:Any?) -> Void)
    {
        if AppUtility.checkNetConnection(){
           
            let headers: HTTPHeaders = [
                "content-type": "application/json"
                
            ]
            request(requestURL, method: HTTPMethod.get.self, parameters: jsonParam, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
                
                printLogger("Request: \(String(describing: response.request))")   // original url request
                printLogger("Response: \(String(describing: response.response))") // http url response
                
                
                if let json = jsonParam
                {
                    printLogger(String(data: try! JSONSerialization.data(withJSONObject: json , options: .prettyPrinted), encoding: .utf8 )!)
                    
                }
                
                LoadingIndicatorView.hide()
                printLogger("Result: \(response.result)")
                
                if let json = response.result.value {
                    printLogger("JSON: \(String(data: try! JSONSerialization.data(withJSONObject: json, options: .prettyPrinted), encoding: .utf8 )!)")
                    
                    
                    if let responseData:Dictionary<String,Any> = json as? Dictionary<String,Any>{
                        completion(true,responseData)
                        return
                    }
                    else if let responseData:Array<Dictionary<String,Any>> = json as? Array<Dictionary<String,Any>>{
                        completion(true,responseData)
                        return
                    }
                    else{
                        if returnInCaseOffailure{
                            completion(false,nil)
                        }
                        else{
                            self.showFailureAlert()
                            completion(false,nil)
                        }
                        return
                    }
                }
                
                if returnInCaseOffailure{
                    completion(false,nil)
                    
                }else{
                    self.showFailureAlert()
                    completion(false,nil)
                }
                return
            }
        }
    }
    */
    
    /*
    class func getApiDataInDict(methodName :String?,headerFieldVal :String?,pathParam :String?,jsonParam:Dictionary<String,Any>?, headers:Dictionary<String, Any>?,requestURL:String,returnInCaseOffailure:Bool,completion : @escaping(_ status:Bool,_ response:Any?) -> Void)
    {
    var request = URLRequest(url: URL(string: requestURL)!)
    request.httpMethod = methodName!
//    request.httpBody = try? JSONSerialization.data(withJSONObject: jsonParam, options: [])
        request.addValue(headerFieldVal!, forHTTPHeaderField: "Content-Type")

    let session = URLSession.shared
    let task = session.dataTask(with: request, completionHandler: { data, response, error -> Void in
        printLogger(response!)
        do {
            let json = try JSONSerialization.jsonObject(with: data!) as! Dictionary<String, AnyObject>
            printLogger(json)
        } catch {
            printLogger("error")
        }
    })

    task.resume()
    }
    */
    
    /*
    class func getApiDataInDict(methodName :String?,headerFieldVal :String?,pathParam :String?,jsonParam:Dictionary<String,Any>?, headers:Dictionary<String, Any>?,requestURL:String,returnInCaseOffailure:Bool,completion : @escaping(_ status:Bool,_ model:MatchDetailModel?) -> Void)
        {
        var request = URLRequest(url: URL(string: requestURL)!)
        request.httpMethod = methodName!
            request.addValue(headerFieldVal!, forHTTPHeaderField: "Content-Type")

        let session = URLSession.shared
        let task = session.dataTask(with: request, completionHandler: { data, response, error -> Void in
            guard let data = data else { return }
            do{
                let model = try JSONDecoder().decode(MatchDetailModel.self, from: data)
                printLogger("Matchdetail Series Name \(String(describing: model.Matchdetail?.Series?.Name))")
                completion(true, model)
            }catch{
printLogger("CATCH")
            }
        })

        task.resume()
        }
    
   */
    
    
    
}


