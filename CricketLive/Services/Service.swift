//
//  Service.swift
//  CricketLive
//
//  Created by Ios_Team on 02/04/21.
//  Copyright © 2021 Techathalon. All rights reserved.
//

import Foundation
class Service {
    
}


extension Data
{
    func getString() -> String
    {
        return String(data: self, encoding: .utf8)!
    }
}
