//
//  MatchDetailTableViewCell.swift
//  CricketLive
//
//  Created by Ios_Team on 01/04/21.
//  Copyright © 2021 Techathalon. All rights reserved.
//

import UIKit

class MatchDetailTableViewCell: UITableViewCell {

    @IBOutlet weak var baseView: DesignableView!
    @IBOutlet weak var lbl_Teams: UILabel!
     @IBOutlet weak var lbl_TourName: UILabel!
    @IBOutlet weak var lbl_Status: UILabel!
    var isDataLoading = false
    @IBOutlet weak var lbl_MatchType: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    var matchModel : MatchDetailModel?{
        didSet{
            
            if let firstTeam = matchModel?.Teams?.teamSix?.Name_Short, let secondTeam = matchModel?.Teams?.teamSeven?.Name_Short{
               self.lbl_Teams.text = "\(firstTeam) VS \(secondTeam)"
            }else{
                self.lbl_Teams.text = "Team 1 VS Team 2"
            }
            self.lbl_TourName.text = matchModel?.Matchdetail?.Series?.Name
            self.lbl_Status.text = matchModel?.Matchdetail?.Result
            self.lbl_MatchType.text = matchModel?.Matchdetail?.Match?.Number
            
            
        }
    }
    
    func addShimmerEffect(){
        let shimmerView = ShimmeringView(frame: CGRect(x: self.baseView.frame.origin.x, y: self.baseView.frame.origin.y, width: self.baseView.frame.maxX, height: self.baseView.frame.maxY))
        self.baseView.addSubview(shimmerView)
        shimmerView.contentView = self.lbl_Teams
        shimmerView.isShimmering = true
        shimmerView.shimmerSpeed = 300
        shimmerView.shimmerPauseDuration = 0.0
    }
}


class ShimmerView: UIView {

    var gradientColorOne : CGColor = UIColor(white: 0.85, alpha: 1.0).cgColor
    var gradientColorTwo : CGColor = UIColor(white: 0.95, alpha: 1.0).cgColor
    
    
    
    func addGradientLayer() -> CAGradientLayer {
        
        let gradientLayer = CAGradientLayer()
        
        gradientLayer.frame = self.bounds
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 1.0)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 1.0)
        gradientLayer.colors = [gradientColorOne, gradientColorTwo, gradientColorOne]
        gradientLayer.locations = [0.0, 0.5, 1.0]
        self.layer.addSublayer(gradientLayer)
        
        return gradientLayer
    }
    
    func addAnimation() -> CABasicAnimation {
       
        let animation = CABasicAnimation(keyPath: "locations")
        animation.fromValue = [-1.0, -0.5, 0.0]
        animation.toValue = [1.0, 1.5, 2.0]
        animation.repeatCount = .infinity
        animation.duration = 0.9
        return animation
    }
    
    func startAnimating() {
        
        let gradientLayer = addGradientLayer()
        let animation = addAnimation()
       
        gradientLayer.add(animation, forKey: animation.keyPath)
    }

}
