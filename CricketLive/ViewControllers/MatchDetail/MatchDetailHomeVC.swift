//
//  MatchDetailHomeVC.swift
//  CricketLive
//
//  Created by Ios_Team on 01/04/21.
//  Copyright © 2021 Techathalon. All rights reserved.
//

import UIKit

class MatchDetailHomeVC: MasterVC {
    
    
    var isDataLoading = false
    @IBOutlet weak var tbl_matchDetailView: UITableView!
    override func viewDidLoad() {
        self.initTableView()
        self.callMatchDetailAPI()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        
    }
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    func initTableView(){
        self.tbl_matchDetailView?.register(UINib(nibName: "MatchDetailTableViewCell", bundle: nil), forCellReuseIdentifier: "MatchDetailTableViewCell")
        self.tbl_matchDetailView.reloadData()
        
    }
    func callMatchDetailAPI(){

//        APIModule.getApiDataInDict(methodName: MethodType.GET.rawValue, headerFieldVal: HeaderValue.Json.rawValue, pathParam: nil, jsonParam: nil, headers: nil, requestURL: kMatchDetailAPI, returnInCaseOffailure: true) { (bool, result) in
//            printLogger(result as Any)
//            if let model : MatchDetailModel = result{
//                self.mainMatchModel = model
//                DispatchQueue.main.async {
//                    self.tbl_matchDetailView.reloadData()
//                }
//            }
//        }
        
//        APIModule.getApiDataInDict(methodName: MethodType.GET.rawValue, headerFieldVal: HeaderValue.Json.rawValue, pathParam: nil, jsonParam: nil, headers: nil, requestURL: kMatchDetailAPI, returnInCaseOffailure: true, decodingType: MatchDetailModel.self) { (flag, result) in
//            if let model : MatchDetailModel = result{
//                self.mainMatchModel = model
//                DispatchQueue.main.async {
//                    self.tbl_matchDetailView.reloadData()
//                }
//            }
//        }
    }
}

extension MatchDetailHomeVC : UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return isDataLoading ? 1 : 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MatchDetailTableViewCell", for: indexPath) as! MatchDetailTableViewCell
//        cell.matchModel = self.mainMatchModel
        cell.addShimmerEffect()
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 130
    }
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        let mainTabBarController = storyboard.instantiateViewController(identifier: "MainTabBarController")
//        kKeyWindows?.rootViewController = mainTabBarController
//        kKeyWindows?.makeKeyAndVisible()
//
//    }
}
