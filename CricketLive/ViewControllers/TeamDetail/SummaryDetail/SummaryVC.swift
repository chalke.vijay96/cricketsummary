//
//  SummaryVC.swift
//  CricketLive
//
//  Created by Ios_Team on 01/04/21.
//  Copyright © 2021 Techathalon. All rights reserved.
//

import UIKit

class SummaryVC: MasterVC {

    @IBOutlet weak var viewOne: UIView!
    @IBOutlet weak var lbl_TeamOne: UILabel!
    @IBOutlet weak var lbl_TeamOneLine: UILabel!
    @IBOutlet weak var btn_TeamOne: UIButton!
    @IBOutlet weak var viewTwo: UIView!
    @IBOutlet weak var lbl_TeamTwo: UILabel!
    @IBOutlet weak var lbl_TeamTwoLine: UILabel!
    @IBOutlet weak var btn_TeamTwo: UIButton!
    
    @IBOutlet weak var tblViews: UITableView!
    @IBOutlet weak var lbl_TopTitle: UILabel!
    var matchViewModel : MatchDetailViewModel? = nil
    var playerArr : [playerDetails]? = nil
    override func viewDidLoad() {
        printLogger("SummaryVC");
        // Do any additional setup after loading the view.
        btn_TeamOne.addTarget(self, action: #selector(btnPressed), for: .touchUpInside)
        btn_TeamTwo.addTarget(self, action: #selector(btnPressed), for: .touchUpInside)
        
        self.initializeTableView()
        self.callCricketMatchModelAPI()
    }
    

    func initializeTableView(){
        self.tblViews?.register(UINib(nibName: "PlayerNameTableViewCell", bundle: nil), forCellReuseIdentifier: "PlayerNameTableViewCell")
        self.tblViews?.register(UINib(nibName: "PlayerHeaderTableViewCell", bundle: nil), forCellReuseIdentifier: "PlayerHeaderTableViewCell")
        
    }
    func setDataInUI(){
        if let teamOneName = self.mainMatchModel?.Teams?.teamSix?.Name_Full, let teamTwoName = self.mainMatchModel?.Teams?.teamSeven?.Name_Full {
            self.lbl_TopTitle.text = "\(teamOneName) VS \(teamTwoName)"
            self.lbl_TeamOne.text = "\(teamOneName)"
            self.lbl_TeamTwo.text = "\(teamTwoName)"
        }
        
    }
    override func viewWillAppear(_ animated: Bool) {
        
    }
    override func viewDidAppear(_ animated: Bool) {
        
    }
    /*
     No need to call API Again can Be Optimized
     */
    func callCricketMatchModelAPI(){
        APIModule.getApiDataInDict(methodName: MethodType.GET.rawValue, headerFieldVal: HeaderValue.Json.rawValue, pathParam: nil, jsonParam: nil, headers: nil, requestURL: kMatchDetailAPI, returnInCaseOffailure: true, decodingType: MatchDetailModel.self) { (flag, result) in
            if let model : MatchDetailModel = result{
                self.mainMatchModel = model
                DispatchQueue.main.async {
                    self.setDataInUI()
                    self.matchViewModel = MatchDetailViewModel(matchModel: self.mainMatchModel)
                    self.btnPressed(sender: self.btn_TeamOne)
                }
            }
        }
        
    }
    @IBAction func btn_BackPress(_ sender: Any) {
        kKeyWindows?.rootViewController = MatchDetailHomeVC()
        kKeyWindows?.makeKeyAndVisible()
    }
    
    @objc private func btnPressed(sender: UIButton) {
        print(sender.tag)
        self.lbl_TeamOneLine.isHidden = (sender.tag == 1)
        self.lbl_TeamTwoLine.isHidden = (sender.tag == 0)
        self.viewOne.backgroundColor = (sender.tag == 0) ? UIColor.white : UIColor.lightGray
        self.viewTwo.backgroundColor = (sender.tag == 1) ? UIColor.white : UIColor.lightGray
        self.playerArr = self.matchViewModel?.getPlayerDetailsArray(forTeam: (sender.tag==0) ? 6 : 7)
        DispatchQueue.main.async {
            self.tblViews.reloadData()
        }
    }
    
}


extension SummaryVC : UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return playerArr?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "PlayerNameTableViewCell", for: indexPath) as! PlayerNameTableViewCell
        cell.setRadiusAndUI()
        cell.playerDetail = playerArr?[indexPath.row]
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 55
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableCell(withIdentifier: "PlayerHeaderTableViewCell") as! PlayerHeaderTableViewCell
        return headerView
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 70
    }
}

