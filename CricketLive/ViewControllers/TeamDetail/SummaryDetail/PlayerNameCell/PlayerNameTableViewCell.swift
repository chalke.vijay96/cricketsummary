//
//  PlayerNameTableViewCell.swift
//  CricketLive
//
//  Created by Ios_Team on 03/04/21.
//  Copyright © 2021 Techathalon. All rights reserved.
//

import UIKit

class PlayerNameTableViewCell: UITableViewCell {
    @IBOutlet weak var lbl_PlayerName: UILabel!
    @IBOutlet weak var captainBaseView: UIView!
    @IBOutlet weak var lbl_Captain: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setRadiusAndUI(){
        self.captainBaseView.layer.cornerRadius = self.captainBaseView.frame.size.height*0.5
    }
    
    var playerDetail : playerDetails?{
        didSet{
            
            if let playerName = playerDetail?.Name_Full{
               self.lbl_PlayerName.text = "\(playerName)"
            }
            self.lbl_Captain.text = (playerDetail?.Iscaptain ?? false) ? "V" : (playerDetail?.Iskeeper ?? false) ? "WK" : ""
           self.captainBaseView.isHidden = !(playerDetail?.Iscaptain ?? false || playerDetail?.Iskeeper ?? false)
            
        }
    }
    
}
