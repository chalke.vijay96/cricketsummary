//
//  MatchDetailModel.swift
//  CricketLive
//
//  Created by Ios_Team on 01/04/21.
//  Copyright © 2021 Techathalon. All rights reserved.
//

import Foundation
import UIKit

class MatchDetailModel : Codable {
//    let user_id: Int?
//    let fullName, userName, email, password: String?
//    let updatedAt, createdAt, login_type: String?
//    let img, status: String?
//    let accessToken, token: String?
    
    var Matchdetail : MatchDetailInfo?
//    var Nuggets : [String]?
//    var Innings : [String]?
    var Teams : Teams?
}

class MatchDetailInfo : Codable {

    var Team_Home : String?
    var Team_Away : String?
    var Match : MatchInfo?
    var Series : SeriesInfo?
    var Venue : VenueInfo?
    var Officials : OfficialsInfo?
    var Weather : String?
    var Tosswonby : String?
    var Status_Id : String?
    var Result : String?
    var Winningteam : String?
    var Winmargin : String?
    var Equation : String?
    
}

class MatchInfo : Codable {
    var Livecoverage : String?
    var Id : String?
    var Code : String?
    var League : String?
    var Number : String?
    var `Type` : String?
    var Date : String?
    var Time : String?
    var Offset : String?
    var Daynight : String?
}
class SeriesInfo : Codable {
    var Id : String?
    var Name : String?
    var Status : String?
    var Tour : String?
    var Tour_Name : String?
    
}
class VenueInfo : Codable {
    var Id : String?
    var Name : String?
}
class OfficialsInfo : Codable {
    var Umpires : String?
    var Referee : String?
}
struct Teams : Codable {
    
    var teamSix : teamSixInfo?
    var teamSeven : teamSevenInfo?
    enum CodingKeys: String, CodingKey {
        case teamSix = "6"
        case teamSeven = "7"
    }
}

//extension Teams {
//        enum CodingKeys: String, CodingKey {
//            case teamSix = "6"
//            case teamSeven = "7"
//        }
//}

class teamSixInfo : Codable {

    var Name_Full : String?
    var Name_Short : String?
    var Players : PlayersInfoTeamSix?
   
}
class PlayersInfoTeamSix : Codable {

    var playerOne : playerDetails?
    var playerTwo : playerDetails?
    var playerThree : playerDetails?
    var playerFour : playerDetails?
    var playerFive : playerDetails?
    var playerSix : playerDetails?
    var playerSeven : playerDetails?
    var playerEight : playerDetails?
    var playerNine : playerDetails?
    var playerTen : playerDetails?
    var playerEleven : playerDetails?
    
    enum CodingKeys: String, CodingKey {
        case playerOne = "63084"
        case playerTwo = "57492"
        case playerThree = "59429"
        case playerFour = "3472"
        case playerFive = "2734"
        case playerSix = "4038"
        case playerSeven = "65739"
        case playerEight = "64073"
        case playerNine = "64321"
        case playerTen = "64306"
        case playerEleven = "66833"
    }
   
}

class teamSevenInfo : Codable {

    var Name_Full : String?
    var Name_Short : String?
    var Players : PlayersInfoTeamSeven?
   
}
class PlayersInfoTeamSeven : Codable {

    var playerOne : playerDetails?
    var playerTwo : playerDetails?
    var playerThree : playerDetails?
    var playerFour : playerDetails?
    var playerFive : playerDetails?
    var playerSix : playerDetails?
    var playerSeven : playerDetails?
    var playerEight : playerDetails?
    var playerNine : playerDetails?
    var playerTen : playerDetails?
    var playerEleven : playerDetails?
    
    enum CodingKeys: String, CodingKey {
        case playerOne = "3667"
        case playerTwo = "4356"
        case playerThree = "12518"
        case playerFour = "28891"
        case playerFive = "5313"
        case playerSix = "59736"
        case playerSeven = "64221"
        case playerEight = "63611"
        case playerNine = "24669"
        case playerTen = "48191"
        case playerEleven = "57458"
    }
   
}


class playerDetails : Codable {
    var Position : String?
    var Name_Full : String?
    var Iscaptain : Bool?
    var Iskeeper : Bool?
    var Batting : BattingDetails?
    var Bowling : BowlingDetails?
    
}

class BattingDetails : Codable {
    
    var Style : String?
    var Average : String?
    var Strikerate : String?
    var Runs : String?
    
}


class BowlingDetails : Codable {
    
    var Style : String?
    var Average : String?
    var Economyrate : String?
    var Wickets : String?
    
}


/*
public struct AnyDecodable: Decodable {
    public let value: Any
    
    public init(_ value: Any?) {
        self.value = value ?? ()
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        
        if container.decodeNil() {
            self.value = ""
        } else if let bool = try? container.decode(Bool.self) {
            self.value = bool
        } else if let int = try? container.decode(Int.self) {
            self.value = int
        } else if let uint = try? container.decode(UInt.self) {
            self.value = uint
        } else if let double = try? container.decode(Double.self) {
            self.value = double
        } else if let string = try? container.decode(String.self) {
            self.value = string
        } else if let array = try? container.decode([AnyDecodable].self) {
            self.value = array.map { $0.value }
        } else if let dictionary = try? container.decode([String: AnyDecodable].self) {
            self.value = dictionary.mapValues { $0.value }
        } else {
            throw DecodingError.dataCorruptedError(in: container, debugDescription: "AnyCodable value cannot be decoded")
        }
    }
}
*/
