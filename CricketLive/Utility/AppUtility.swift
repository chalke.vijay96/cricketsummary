//
//  AppUtility.swift
//  CricketLive
//
//  Created by Ios_Team on 02/04/21.
//  Copyright © 2021 Techathalon. All rights reserved.
//

import Foundation
import UIKit
class AppUtility: NSObject{

    class func hexStringToUIColor (hex:String) -> UIColor {
      
      
          var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
          
          if (cString.hasPrefix("#")) {
              cString.remove(at: cString.startIndex)
          }
          
          if ((cString.count) != 6) {
              return UIColor.gray
          }
          
          var rgbValue:UInt32 = 0
          Scanner(string: cString).scanHexInt32(&rgbValue)
          
          return UIColor(
              red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
              green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
              blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
              alpha: CGFloat(1.0)
          )
      }
    class  func displayAlert(message:String)
      {
          let alert = UIAlertController(title: "Alert", message: message, preferredStyle: UIAlertController.Style.alert)
          alert.addAction(UIAlertAction(title: AlertMessage.TITLE_OK, style: UIAlertAction.Style.default, handler: nil))
        kKeyWindows?.rootViewController?.present(alert, animated: true, completion: nil)
      }
      
      
      class func displayAlertController(title:String , message:String) {
          
          
          let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
          alert.addAction(UIAlertAction(title: AlertMessage.TITLE_OK, style: UIAlertAction.Style.default, handler: nil))
          kKeyWindows?.rootViewController?.present(alert, animated: true, completion: nil)
          
      }

      class func displayAlert(title:String , message:String) {
          
          
          let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
          alert.addAction(UIAlertAction(title: AlertMessage.TITLE_OK, style: UIAlertAction.Style.default, handler: nil))
          if let tc = UIApplication.shared.keyWindow?.rootViewController as? UITabBarController {
              if let navigationVc =  tc.selectedViewController as? UINavigationController
              {
                  navigationVc.topViewController?.present(alert, animated: true, completion: nil)
              }
          }
      }
    @objc class func checkNetConnection() -> Bool
    {
        
        if (NetworkReachabilityManager.init()?.isReachable)! {
            
            return true
            ///printLog("Reachable")
        }
        else
        {
            AppUtility.displayAlert(message: AlertMessage.CHECK_NET_CONNECTION)
            return false
            
        }
    }
}
