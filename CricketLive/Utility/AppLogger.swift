//
//  AppLogger.swift
//  CricketLive
//
//  Created by Ios_Team on 02/04/21.
//  Copyright © 2021 Techathalon. All rights reserved.
//

import Foundation
import UIKit

struct AppLogger {
    static let isLogsEnabled: Bool = true
}

public func printLogger(_ items: Any...)  {
    if AppLogger.isLogsEnabled {
        if  items.count > 0{
            for item in items {
                print(item)
            }
        }
    }
}
