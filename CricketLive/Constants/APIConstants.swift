//
//  APIConstants.swift
//  CricketLive
//
//  Created by Ios_Team on 02/04/21.
//  Copyright © 2021 Techathalon. All rights reserved.
//

import Foundation

// --------- Web services ------------
let kMatchDetailAPI       = "https://cricket.yahoo.net/sifeeds/cricket/live/json/sapk01222019186652.json"

// --------- Titles ------------
public enum MethodName : String {
    case LOGIN = "login"
    case REGISTER = "signup"
}
