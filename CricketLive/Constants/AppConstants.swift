//
//  AppConstants.swift
//  CricketLive
//
//  Created by Ios_Team on 02/04/21.
//  Copyright © 2021 Techathalon. All rights reserved.
//

import Foundation
import UIKit
let kAppName = "CanDo"
var kWindow = UIWindow(frame: UIScreen.main.bounds)
let kApplicationDelegate = UIApplication.shared.delegate as! AppDelegate
var kScreenSize = UIScreen.main.bounds.size
var kKeyWindows = UIApplication.shared.keyWindow
let kUserDefault = UserDefaults.standard
let kFrameWidth = UIScreen.main.bounds.size.width
let kFrameHeight = UIScreen.main.bounds.size.height

struct AlertMessage{
    static let CHECK_NET_CONNECTION = "Please check your internet connection"
    static let VERSION_UPGRADE_MSG = "A new version is available. Please upgrade your application"
    static let NO_DATA = "No data found !"
    static let NO_DATA_TITLE = "No data"
    static let TITLE_OK = "Ok"
    static let TITLE_Alert = "Alert"
    static let TITLE_REMARKS = "Remarks"
    static let TITLE_DONE = "Done"
    static let TITLE_CANCEL = "Cancel"
    static let TITLE_TRY_AGAIN = "Try Again"
    
}

struct UserDefaultConstant{
    static let savedMatchModel = "Saved_MatchModel"
    
}
