//
//  MatchDetailViewModel.swift
//  CricketLive
//
//  Created by Ios_Team on 02/04/21.
//  Copyright © 2021 Techathalon. All rights reserved.
//

import Foundation
import UIKit

class MatchDetailViewModel{
    var matchModel : MatchDetailModel?
    init(matchModel : MatchDetailModel?) {
        self.matchModel = matchModel
        printLogger( matchModel?.Teams?.teamSix?.Name_Full as Any)
    }
    func getPlayerDetailsArray(forTeam:Int)->[playerDetails]?{

        if (forTeam == 6){
            if let teamSixPlayers = self.matchModel?.Teams?.teamSix?.Players{
                let finalPlayerArr = [teamSixPlayers.playerOne,teamSixPlayers.playerTwo,teamSixPlayers.playerThree,teamSixPlayers.playerFour,teamSixPlayers.playerFive,teamSixPlayers.playerSix,teamSixPlayers.playerSeven,teamSixPlayers.playerEight,teamSixPlayers.playerNine,teamSixPlayers.playerTen,teamSixPlayers.playerEleven]
                return finalPlayerArr as? [playerDetails]
            }else{
                return nil
            }
        }else{
            if let teamSixPlayers = self.matchModel?.Teams?.teamSeven?.Players{
                let finalPlayerArr = [teamSixPlayers.playerOne,teamSixPlayers.playerTwo,teamSixPlayers.playerThree,teamSixPlayers.playerFour,teamSixPlayers.playerFive,teamSixPlayers.playerSix,teamSixPlayers.playerSeven,teamSixPlayers.playerEight,teamSixPlayers.playerNine,teamSixPlayers.playerTen,teamSixPlayers.playerEleven]
                return finalPlayerArr as? [playerDetails]
            }else{
                return nil
            }
        }
    }
}
