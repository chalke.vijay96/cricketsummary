//
//  IBDesignable.swift
//  CricketLive
//
//  Created by Ios_Team on 01/04/21.
//  Copyright © 2021 Techathalon. All rights reserved.
//

import Foundation
import UIKit
import QuartzCore

@IBDesignable
open class DesignableView : UIView {
    
    let gradientLayer = CAGradientLayer()
    
    @IBInspectable
    var topGradientColor: UIColor? {
        didSet {
            
        }
    }
    
    @IBInspectable
    var bottomGradientColor: UIColor? {
        didSet {
            
        }
    }
    
    override open func layoutSubviews() {
        super.layoutSubviews()
        
        
        if let topGradientColor = topGradientColor, let bottomGradientColor = bottomGradientColor {
            self.gradientLayer.frame = bounds
            gradientLayer.colors = [topGradientColor.cgColor, bottomGradientColor.cgColor]
            gradientLayer.borderColor = layer.borderColor
            gradientLayer.borderWidth = layer.borderWidth
            gradientLayer.cornerRadius = layer.cornerRadius
            layer.insertSublayer(gradientLayer, at: 0)
        } else {
            gradientLayer.removeFromSuperlayer()
        }
        
    }
    
    @IBInspectable var newcornerRadius: Double {
        get {
            return Double(self.layer.cornerRadius)
        }set {
            self.layer.cornerRadius = CGFloat(newValue)
        }
    }
    @IBInspectable var newborderWidth: Double {
        get {
            return Double(self.layer.borderWidth)
        }
        set {
            self.layer.borderWidth = CGFloat(newValue)
        }
    }
    //    @IBInspectable var newborderColor: UIColor? {
    //         get {
    //            return UIColor(cgColor: self.layer.borderColor!)
    //         }
    //         set {
    //            self.layer.borderColor = newValue?.cgColor
    //         }
    //    }
    @IBInspectable var newshadowColor: UIColor? {
        get {
            return UIColor(cgColor: self.layer.shadowColor!)
        }
        set {
            self.layer.shadowColor = newValue?.cgColor
        }
    }
    
    @IBInspectable
    var newshadowRadius: CGFloat {
        get {
            return layer.shadowRadius
        }
        set {
            layer.shadowRadius = newValue
        }
    }
    
    @IBInspectable var newshadowOpacity: Float {
        get {
            return self.layer.shadowOpacity
        }
        set {
            self.layer.shadowOpacity = newValue
        }
    }
}

